from django.contrib import admin
from django.urls import path, include
from blog import views

urlpatterns = [
    path("", views.redirect_view),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('blog/', include('blog.urls')),
]


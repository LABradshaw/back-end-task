from django.db import models
from datetime import datetime


class Library(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=600)

	def __str__(self):
		return self.name

class Article(models.Model):
	title = models.CharField(max_length=150)
	content = models.CharField(max_length=10000)
	author = models.CharField(max_length=50)
	date_created = models.DateTimeField(editable=False)
	last_modified = models.DateTimeField(null=True)

	"""Add a ref to parent 'Shelf'.

	Find out how to set the value to be a str of the name of
	the parent. If when the parent object is deleted the value can be passed before
	the info is removed from the db.

	Otherwise a str of the name and id of parent can be added and remains
	after parent is deleted."""

	 # shelf = models.ForeignKey(Library, on_delete=models.SET(parent_deleted), default=Library.id)


	# Create or Update timestamps
	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = datetime.utcnow()
		self.last_modified = datetime.utcnow()

		super().save(*args, **kwargs)


	def __str__(self):
		return f"{self.title} - {self.date_created} "


class Comment(models.Model):
	article_id = models.ForeignKey(Article, on_delete=models.DO_NOTHING, default=Article.objects.all()[0])
	content = models.CharField(max_length=1000)
	author = models.CharField(max_length=50, default="Anonymous")
	date_created = models.DateTimeField(editable=False)

	# Planning to hash user input to allow anon posters to validate identity
	validate_identity = models.CharField(max_length=64, null=False)

	"""Could add parent/child comments for a reply feature
		parent_id = models.ForeignKey(Comment, on_delete=models.DO_NOTHING)
	"""
	# Modified save func
	def save(self, *args, **kwargs):
		if not self.id:
			self.date_created = datetime.utcnow()

		super().save(*args, **kwargs)


	def __str__(self):
		return f"{self.author} at {self.date_created}"

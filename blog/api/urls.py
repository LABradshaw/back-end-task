from django.conf.urls import url
from django.urls import path, include
from .views import ArticleApiView, CommentApiView


urlpatterns = [
    path('articles/', ArticleApiView.as_view()),
    path('comments/', CommentApiView.as_view()),
]
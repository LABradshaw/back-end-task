from rest_framework import serializers
from blog.models import Library, Article, Comment

class ArticleSerializer(serializers.ModelSerializer):
	class Meta:
		model = Article
		fields = ['title', 'content', 'author', 'date_created', 'last_modified']

class CommentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Comment
		fields = ['content', 'author', 'date_created']
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# from rest_framework import permissions # look into difference in this and built in permissions
from blog.models import Article, Comment
from. serializers import ArticleSerializer, CommentSerializer
from django.contrib.admin.views.decorators import staff_member_required


class ArticleApiView(APIView):

	# List
	def get(self, request, *args, **kwargs):
		articles = Article.objects.all()
		serializer = ArticleSerializer(articles, many=True)
		return Response(serializer.data, status=status.HTTP_200_OK)


	# Create
	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return Response("login to post", status.HTTP_401_UNAUTHORIZED)

		data = {
			"title": request.data.get("title"),
			"content": request.data.get("content"),
			"author": request.data.get("author"),
			"date created": request.data.get("date created"),
			"last modified": request.data.get("last modified")
		}

		serializer = ArticleSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentApiView(APIView):

	# List
	def get(self, request, *args, **kwargs):
		comments = Comment.objects.all()
		serializer = CommentSerializer(comments, many=True)
		return Response(serializer.data, status=status.HTTP_200_OK)

	# Create

	def post(self, request, *args, **kwargs):
		data = {
			"article": request.data.get("article"),
			"content": request.data.get("content"),
			"author": request.data.get("author"),
			"date created": request.data.get("date created"),
		}

		if not data["article"]:
			data["article"] = Article.objects.all()[0]
			
		if request.user.is_authenticated:
			data["author"] = request.user.username

		serializer = CommentSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
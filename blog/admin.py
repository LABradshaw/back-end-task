from django.contrib import admin

from .models import Library, Article, Comment

admin.site.register(Library)
admin.site.register(Article)
admin.site.register(Comment)

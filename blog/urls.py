from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.home),
    path('api/', include('blog.api.urls')),
]
